var values: string[] = [];  // initializing array

function addRecord() {
    var inp = (document.getElementById("inputtext") as HTMLInputElement);
    values.push(inp.value); //assign user input to array
    inp.value = "";
    document.getElementById("values").innerHTML = values.join(", "); //display user input on button click
}

function sortAsc() {
    values.sort((a, b) => 0 - (a > b ? -1 : 1)); //sort array in ascending order
    document.getElementById("ascvalues").innerHTML = values.join(", ");    //display ascending order sort
}

 
function sortDesc() {
    values.sort((a, b) => 0 - (a > b ? 1 : -1));  //sort array in descending order
    document.getElementById("descvalues").innerHTML = values.join(", "); //display descending order sort
}